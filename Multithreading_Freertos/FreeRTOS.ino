#include "Arduino.h"
typedef void (*function_list)(void*);

#define RUNNING_CORE_NUMBER_ONE 0
#define RUNNING_CORE_NUMBER_TWO 1

SemaphoreHandle_t xSemaphore;

void setup() {

	Serial.begin(115200);

	function_list taskslist[] = { taskBlink, taskUartSendData };

	xTaskCreatePinnedToCore(taskslist[0], "TaskBlink", 2068, NULL,
			configMAX_PRIORITIES - 1, NULL, RUNNING_CORE_NUMBER_ONE);
#ifdef EXPERIMENT
	xSemaphore = xSemaphoreCreateMutex();
	xTaskCreatePinnedToCore(taskslist[1], "taskUartSendData", 2068, NULL,
			configMAX_PRIORITIES - 1, NULL, RUNNING_CORE_NUMBER_ONE);
#else
	xTaskCreatePinnedToCore(taskslist[1], "taskUartSendData", 2068, NULL,
			configMAX_PRIORITIES - 1, NULL, RUNNING_CORE_NUMBER_TWO);
#endif
}

void loop() {
}

void taskBlink(void*) {
	String taskMessage = "TaskBlink running on core: ";
	taskMessage = taskMessage + xPortGetCoreID();
#ifdef EXPERIMENT
	xSemaphoreTake(xSemaphore, (TickType_t) 10);
#endif
	while (1) {
		Serial.println(taskMessage);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}

void taskUartSendData(void*) {
	String taskMessage = "TaskUartSendData running on core: ";
	taskMessage = taskMessage + xPortGetCoreID();
#ifdef EXPERIMENT
	xSemaphoreTake(xSemaphore, (TickType_t) 10);
#endif
	while (1) {
		Serial.println(taskMessage);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}
