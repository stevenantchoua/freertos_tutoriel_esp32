#include "freertos/queue.h"
#include "Arduino.h"

QueueHandle_t xQueue1;

void setup() {
	Serial.begin(115200);

	xQueue1 = xQueueCreate(10, sizeof(struct AMessage *));

	if (xQueue1 == NULL) {
		printf("Queue don't be created");
		ESP.restart();
	}

	xTaskCreate(vTaskSendData, "sendData", 2048, NULL, 3, NULL);
	xTaskCreate(vTaskReceiveData, "ReceiveData", 2048, NULL, 3, NULL);

}

void loop() {

#ifdef DEADLOCK
	printf("Yes Mon bon Petit\n");
#endif
}

struct AMessage {
	uint8_t ucMessageID;
	char ucData[15];
};

void vTaskSendData(void*) {
	uint8_t id = 12;
	AMessage senddata = { id, "HelloWorld" };

	while (1) {
		if (xQueue1 != NULL) {
			printf("ID: %d , Message envoyee: %s \n", senddata.ucMessageID,
					senddata.ucData);
			flag = xQueueSend(xQueue1, (void * ) &senddata, (TickType_t ) 0);
			id++;
			senddata.ucMessageID = id;
			printf("etat du flag: %s \n", flag ? "true" : "false");
#ifdef CREATEVSPINCORE
			printCurrentCore("vTaskSendData running on core:");
#endif
			vTaskDelay(2000);
		}
	}
}

void vTaskReceiveData(void*) {
	AMessage receivedata;

	while (1) {
			xQueueReceive(xQueue1, &receivedata, (TickType_t ) 10);
			printf("ID: %d , Message recu: %s \n", receivedata.ucMessageID,
					receivedata.ucData);
#ifdef CREATEVSPINCORE
			printCurrentCore("vTaskReceiveData running on core:");
#endif
			vTaskDelay(2000);
	}
}

void printCurrentCore(String message) {
	message = message + xPortGetCoreID();
	Serial.println(message);
}
