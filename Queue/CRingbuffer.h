/*
 * CRingbuffer.h
 *
 *  Created on: 07.06.2018
 *  Author: steve Thierry,Nantchoua
 *  contact: www.makeingenieur.com
 */

#ifndef CRINGBUFFER_H_
#define CRINGBUFFER_H_

#include <iostream>
#include <string>
using namespace std;

template<typename T, unsigned long long int size>
class CRingbuffer {
private:
	/*
	 * m_iw= index d'ecriture
	 * m_idr= index de lecture
	 * m_currentcilled = nombre d'element enregistrer
	 * */
	unsigned long int m_currentcilled;
	T *m_data;
	unsigned long long int m_idw, m_idr;


	unsigned long long int next(unsigned long long int idx) {
		return (idx <= size) ? idx++ : 0;
	}
	void pop(unsigned long long int index) {
			if (!empty()) {
				m_data[index]=0;
				std::cout << "Data with index"<<index<<" is deleted " << endl;
			} else {
				std::cout << "Data with index"<<index<<" don't find! " << endl;
			}
		}
public:
	CRingbuffer() {
		m_idr = m_idw = 0;
		m_currentcilled = 0;
		try {
			m_data = new T[size];

		} catch (...) {
			std::cout << "malloc Exception , please free the "
					"memory on the Heap and try again \n" << std::endl;
		}
	}
	virtual ~CRingbuffer() {
		if (m_currentcilled > 0) {
			delete[] m_data;
			std::cout << "Object with adress"<<this<<" has be deleted" << "\n";
		}
	}

	bool full() {
		return this->next(m_idw) == this->m_idr;
	}

	bool empty() {
		return this->m_idr == this->m_idw;
	}

	void pusch(T data) {
		if (!full()) {
			this->m_data[m_idw++] = data;
			std::cout << "Data with index:"<<m_idw<<" is writed" << endl;
			m_currentcilled++;
		}
	}

	T read() {
		T data;
			if (empty() == false) {
				data = m_data[m_idr];
				pop(m_idr);
				m_idr++;
				m_currentcilled--;
			}
			return data;
	}
};
#endif /* CRINGBUFFER_H_ */
