SemaphoreHandle_t xSemaphore;
BaseType_t xReturned;
TaskHandle_t xHandle;

void setup() {

	Serial.begin(115200);

	xReturned = xTaskCreate(vTaskexemple, "check", 2048, (void*) 1, 5,
			&xHandle);
	if (xReturned == true) {
		Serial.println("Task cree");
	} else {
		Serial.println("Task non cree");
	}
}

void loop() {
	Serial.println("Hello World");
	vTaskDelay(2000);
}

void vTaskexemple(void * pvParameters) {
	xSemaphore = xSemaphoreCreateBinary();
	printf("Parametre: %d\n", (int) pvParameters);

	while (1) {
#ifdef TEST
		xSemaphoreTake(xSemaphore, (TickType_t) 10);
#endif
		Serial.println("Task en cour d'execution");
		for (long i = 0; i <= 10000000000000; i++)
			;
#ifdef SOLVETEST
		xSemaphoreGive(xSemaphore);
#endif
	}
}


